import {MigrationInterface, QueryRunner} from "typeorm";

export class CrateFileTable1626439127982 implements MigrationInterface {
    name = 'CrateFileTable1626439127982'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "file" ("fId" SERIAL NOT NULL, "title" character varying NOT NULL, "description" character varying NOT NULL, "filePath" character varying NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "status" integer NOT NULL DEFAULT 1, "featured" boolean NOT NULL, CONSTRAINT "PK_c95947d4b014f9e98e1ad0616c8" PRIMARY KEY ("fId"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "file"`);
    }

}
