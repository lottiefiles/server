import { Factory, Seeder } from 'typeorm-seeding';
import { Connection } from 'typeorm';

export default class CreateUsers implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
    await connection
      .createQueryBuilder()
      .insert()
      .into('file')
      .values([
        {
          title: 'Store Grow Up',
          description: 'Store Grow Up',
          filePath: '9596-stone-grow-up.json',
          featured: false,
        },
        {
          title: 'Dark Grow Up',
          description: 'Dark Grow Up',
          filePath: '9597-dark-grow-up.json',
          featured: true,
        },
        {
          title: 'Haloween Text Loading',
          description: 'Haloween Text Loading',
          filePath: '9941-halloween-text-loading.json',
          featured: false,
        },
        {
          title: 'Halloween Pumpkin Black Cat',
          description: 'Halloween Pumpkin Black Cat',
          filePath: '9969-halloween-pumpkin-black-cat.json',
          featured: true,
        },
        {
          title: 'Home Icon Loading',
          description: 'Home Icon Loading',
          filePath: '10160-home-icon-loading.json',
          featured: false,
        },
        {
          title: 'Message Inbox Notification',
          description: 'Message Inbox Notification',
          filePath: '10255-message-inbox-notification.json',
          featured: true,
        },
        {
          title: 'Clock Alarm Animation',
          description: 'Clock Alarm Animation',
          filePath: '10256-clock-alarm-animation.json',
          featured: false,
        },
        {
          title: 'Icon Set Compass',
          description: 'Icon Set Compass',
          filePath: '10449-icon-set-compass.json',
          featured: true,
        },
        {
          title: 'Icon Set Setting',
          description: 'Icon Set Setting',
          filePath: '10450-icon-set-setting.json',
          featured: false,
        },
        {
          title: 'Christmas Snow Loading',
          description: 'Christmas Snow Loading',
          filePath: '12131-christmas-snow-loading.json',
          featured: true,
        },
        {
          title: 'Christmas Loading',
          description: 'Christmas Loading',
          filePath: '12358-christmas-loading.json',
          featured: false,
        },
        {
          title: 'App Rating',
          description: 'App Rating',
          filePath: '/27210-app-rating.json',
          featured: true,
        },
      ])
      .execute();
  }
}
