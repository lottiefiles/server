import {Field, Float, ID, InputType, Int} from '@nestjs/graphql';

@InputType()
export class FileDetailsInput {
  @Field(() => Int)
  fId: number;
}
