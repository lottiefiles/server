import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class ListFilesInput {
  /**
   * this parameter will take the search string to fitler the files
   */
  @Field({ nullable: true })

  /**
   * This paramters is either to show featured or recent files
   */
  search: string;
  @Field({ nullable: true })
  filter: string;
}
