import { Inject } from '@nestjs/common';
import { Resolver, Query, Args, Mutation, ID } from '@nestjs/graphql';
import { File } from '../schemas/files.schema';
import { FilesService } from '../services/files.service';
import { ListFilesInput } from '../inputs/list-files-input';
import {FileDetailsInput} from "../inputs/file-details-input";

@Resolver(File)
export class FilesResolver {
  constructor(
    @Inject(FilesService) private readonly fileService: FilesService
  ) {}

  /**
   * The files resolver to get list of all files based on search and filters
   *
   * @param data ListFilesInput
   * @returns [File]
   */
  @Query((retunrs) => [File])
  async files(@Args('data') data: ListFilesInput) {
    return await this.fileService.find(data);
  }

  /**
   *  This function is to get on file it acepts fId in data param and return the file
   *
   * @param data FileDetailsInput
   * @returns File
   */
  @Query((retunrs) => File)
  async file(@Args('data') data: FileDetailsInput) {
    return await this.fileService.findOne(Number(data.fId));
  }
}
