import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Field, ObjectType, ID } from '@nestjs/graphql';

enum Status {
  active,
  deleted,
}

@Entity()
@ObjectType()
export class File {
  @Field(() => ID)
  @PrimaryGeneratedColumn('increment')
  fId: number;

  @Field(() => String)
  @Column()
  title: string;

  @Field(() => String)
  @Column()
  description: string;

  @Field(() => String)
  @Column()
  filePath: string;

  @Field(() => Date)
  @CreateDateColumn()
  createdAt: Date;

  @Field(() => Date)
  @UpdateDateColumn()
  updatedAt: Date;

  @Field(() => Number)
  @Column('int', { default: 1 })
  status: Status = 1;

  @Field(() => Boolean)
  @Column()
  featured: Boolean = false;
}
