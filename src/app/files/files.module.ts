import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { FilesService } from './services/files.service';
import { FilesResolver } from './resolvers/files.resolver';
import { File } from './schemas/files.schema';

@Module({
  imports: [TypeOrmModule.forFeature([File])],
  providers: [FilesService, FilesResolver],
})
export class FilesModule {}
