import { Injectable } from '@nestjs/common';
import { Repository, Like, MoreThanOrEqual } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { File } from '../schemas/files.schema';
import { ListFilesInput } from '../inputs/list-files-input';
import * as moment from 'moment';

@Injectable()
export class FilesService {
  constructor(
    @InjectRepository(File) private readonly fileRepository: Repository<File>,
  ) {}

  /**
   * This will take data the contains search and filter and get the data from db and return it
   *
   * @param data ListFilesInput
   * @returns
   */
  public async find(data: ListFilesInput): Promise<Array<File>> {
    const options: any = {};

    if (data.search || data.filter) {
      options.where = {};
    }

    if (data.search) {
      options.where.title = Like('%' + data.search + '%');
    }

    if (data.filter === 'featured') {
      options.where.featured = true;
    }

    if (data.filter === 'recent') {
      options.where.updatedAt = MoreThanOrEqual(
        moment().subtract(1, 'days').format('YYYY-MM-DD hh:mm:ss'),
      );
    }

    return await this.fileRepository.find(options);
  }

  /**
   * This function it to get file with id
   *
   * @param id number
   * @return File object
   */
  public async findOne(id: number): Promise<File> {
    return await this.fileRepository.findOne(id)
  }
}
