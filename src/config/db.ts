import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { registerAs } from '@nestjs/config';
import { config as setConfig } from 'dotenv';

setConfig();
export default registerAs(
  'typeOrmConfig',
  (): TypeOrmModuleOptions => ({
    type: 'postgres',
    host: process.env.POSTGRES_HOST || 'localhost',
    port: Number(process.env.POSTGRES_PORT) || 5432,
    username: process.env.POSTGRES_USER || 'test',
    password: process.env.POSTGRES_PASSWORD || 'test',
    database: process.env.POSTGRES_DATABASE || 'test',
    entities: ['dist/**/*.schema{.ts,.js}'],
    synchronize: false,
    cli: {
      migrationsDir: 'src/migrations',
    },
    migrations: ['dist/migrations/**/*.js'],
    migrationsRun: false,
  }),
);
