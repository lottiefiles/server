# LottieFiles Backend

## Setup
```
git clone git@gitlab.com:lottiefiles/server.git
cd server
```

## Running the server
```
docker-compose up -d
```

## Opening the graphql GUI
```
http://localhost:3001/graphql
```


## Run the migrations
```
docker-compose exec api yarn migrations:migrate
```

## Run the seender
```
docker-compose exec api yarn db:seed
```
