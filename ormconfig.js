require('dotenv').config()

module.exports = {
  type: 'postgres',
  host: process.env.POSTGRES_HOST || 'localhost',
  port: Number(process.env.POSTGRES_PORT) || 5432,
  username: process.env.POSTGRES_USER || 'test',
  password: process.env.POSTGRES_PASSWORD || 'test',
  database: process.env.POSTGRES_DATABASE || 'test',
  entities: ['dist/**/*.schema{.ts,.js}'],
  migrations: ['dist/migrations/**/*{.ts,.js}'],
  seeds: ['src/seeds/**/*.seed{.ts,.js}'],
}
