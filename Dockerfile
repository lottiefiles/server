FROM node:14-alpine

ENV PORT 3001

# Create app directory
RUN mkdir -p /app
WORKDIR /app

# Copy
COPY ./ /app/

# Installing dependencies
COPY package*.json /app/
COPY yarn.lock /app/
RUN yarn install

# Build
RUN yarn build


# migrating the db
#RUN yarn migration:migrate

# Seeding the db
#RUN yarn db:seed


EXPOSE 3001

# Running the app
CMD "yarn" "start"

